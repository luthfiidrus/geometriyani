extends LinkButton

export(String) var scene_to_load

func _on_LinkButton_pressed():
	if global.death_count != global.count_before_death:
		global.count_before_death = global.death_count
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
