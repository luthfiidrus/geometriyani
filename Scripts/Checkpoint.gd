extends Area2D

var not_visited = preload("res://Assets/Flag/not_visited.png")
var visited = preload("res://Assets/Flag/visited.png")

export(int) var index

func _ready():
	var mysprite = get_node("Sprite")
	if global.checkpoint[index] == true:
		mysprite.set_texture(visited)
	else:
		mysprite.set_texture(not_visited)

func _on_Checkpoint_body_entered(body):
	global.checkpoint[index] = true
	var mysprite = self.get_node("Sprite")
	mysprite.set_texture(visited)
