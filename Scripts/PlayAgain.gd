extends LinkButton

export(String) var scene_to_load

func _on_LinkButton_pressed():
	global.death_count = 0
	global.count_before_death = 0
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
