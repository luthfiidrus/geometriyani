extends KinematicBody2D

export (int) var speed = 500
export (int) var jump_speed = -450
export (int) var GRAVITY = 1200
export (bool) var fly = false

var bullet = preload("res://Scenes/Bullet.tscn")

const UP = Vector2(0,-1)

var velocity = Vector2()

var can_fire = true
var rate_of_fire = 0.4

func _process(delta):
	shoot_loop()
	
func shoot_loop():
	if Input.is_action_pressed("Shoot") and can_fire == true:
		can_fire = false
		get_node("TurnAxis").rotation = get_angle_to(get_global_mouse_position())
		var bullet_instance = bullet.instance()
		bullet_instance.position = get_node("TurnAxis/CastPoint").get_global_position()
		bullet_instance.rotation = get_angle_to(get_global_mouse_position())
		get_parent().add_child(bullet_instance)
		yield(get_tree().create_timer(rate_of_fire), "timeout")
		can_fire = true
	
func get_input():
	if is_on_floor():
		speed = 500
	if fly == false:
		if Input.is_action_just_pressed("ui_select") and is_on_floor():
			velocity.y = jump_speed
			speed = speed - 50
	else:
		if Input.is_action_just_pressed("ui_select"):
			velocity.y = jump_speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	velocity.x = speed
	get_input()
	velocity = move_and_slide(velocity, UP)

func _on_Portal1_body_exited(body):
	fly = true
	GRAVITY = 1500


func _on_Portal2_body_exited(body):
	fly = false
	GRAVITY = 1200
