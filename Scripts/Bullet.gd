extends RigidBody2D

var projectile_speed = 1000
var life_time = 2

func _ready():
	apply_impulse(Vector2(), Vector2(projectile_speed, 0).rotated(rotation))
	terminate()

func terminate():
	yield(get_tree().create_timer(2), "timeout")
	queue_free()


func _on_Bullet_body_entered(body):
	self.hide()
	queue_free()
