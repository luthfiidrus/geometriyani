extends Area2D

export (String) var sceneName = "Lose"

func _on_Spike_body_entered(body):
	if body.get_name() == "Player":
		if global.death_count == global.count_before_death:
			global.death_count += 1
		else:
			global.count_before_death = global.death_count
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
