extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if global.checkpoint[0] == true:
		$Player.set_position($Checkpoint1.get_position())
	if global.checkpoint[1] == true:
		$Player.set_position($Checkpoint2.get_position())

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
